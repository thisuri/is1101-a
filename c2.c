#include<stdio.h>
//This program will computes the area of a disk

int main()
{
	float r,a; //r=radius a=area
	const double PI=3.14;
	printf("Enter the radius of the circle \n");
	scanf("%f",&r);
	a=PI*r*r;
	printf("radius= %.2f\narea of the disk=%f \n",r,a);
	return 0;
}
