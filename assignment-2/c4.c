#include<stdio.h>
/*This program takes a number from a user 
and checks whether that number is either positive 
negative or zero*/

int main()
{
	int no;
	printf("Enter a number \n");
	scanf("%d",&no);
	printf("%d is ",no);
	if(no>0)
		printf("a positive number \n");
	else if(no<0)
		printf("a negative number \n");
	else
		printf("zero \n");
	return 0;				
}
