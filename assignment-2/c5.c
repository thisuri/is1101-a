#include<stdio.h>
/*This program takes a number from the user 
and checks whether that number is odd or even*/

int main()
{
	int no;
	printf("Enter a number \n");
	scanf("%d",&no);
	printf("%d is ",no);
	switch(no%2)
	{
		case 0:
			printf("an even number \n");
			break;
		case 1:	
			printf("an odd number \n");
			break;
	}
	return 0;
}
