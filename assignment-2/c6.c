#include<stdio.h>
//This program checks a given character is Vowel or Consonant

int main()
{
	char l;
	printf("Enter a letter \n");
	scanf("%c",&l);
	printf("%c is ",l);
	if (l>64&&l<91||l>96&&l<123)
	{
		switch(l)
		{
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
			case 'A':
			case 'E':
			case 'I':
			case 'O':
			case 'U':
				printf("a Vowel \n");
				break;
			default:
				printf("a constant \n");	
		}
	}	
	else
		printf("a non-alphabetic character \n");	
	return 0;
}
