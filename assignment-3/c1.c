#include<stdio.h>
/*This program will calculate the sum of all given positive integers
 until we enter the 0 or a negative value*/
 
 int main()
 {
 	int no,tot;
 	
 	do{
 		printf("Enter a positive number \n");
		scanf("%d",&no);
		
		if(no<0)
		{
			printf("you should enter a positive number \n");
			continue;
		}
		
		else if(no==0)
			break;	
		
		tot+=no;	//tot=total
		
	}while(1);
	
	printf("Total = %d",tot);
	
	return 0;
 }
