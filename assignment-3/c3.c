#include<stdio.h>
//This program will reverse a Number

int main()
{
	int no,lastdig,reverse=0;
	
	printf("Enter a number \n");
	scanf("%d",&no);
	
	while(no>0)
	{
		lastdig=no%10;
		
		reverse=(reverse*10)+lastdig;
		
		no=no/10;
	}
	
	printf("Reversed number = %d",reverse);
	
	return 0;
}
