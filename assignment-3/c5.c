#include<stdio.h>
/*This program will a print multiplication tables from 1 to n
for a given an integer value n*/

int main()
{
	int n,x,y;
	
	printf("Enter a number \n");
	scanf("%d",&n);
	
	for(x=1;x<=n;x++)
	{
		for(y=1;y<=10;y++)
		{
			printf("%d * %d = %d \n",x,y,x*y);
		}
		printf("\n");
	}
	
	return 0;
}
