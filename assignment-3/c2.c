#include<stdio.h>
//This program will determine the given number is a prime number

int main()
{
	int no,x,count=0;
	
	printf("Enter a number \n");
	scanf("%d",&no);
	
	for(x=1;x<=no;x++)
	{
		if(no%x==0)	
			count++;	
	}
	
	if(count>2)
		printf("%d is a composite number \n",no);
	
	else if(count==2)
		printf("%d is a prime number \n",no);
	
	else
		printf("%d is not a prime or composite number",no);		
	
	return 0;			
}
